
import os
import uuid
import random
from typing import Union

from tqdm import tqdm
import numpy as np
import rasterio

from src.img_loader import load_frame
from src.coord_handler import generate_coords


class MoasicAugmentator:
    def __init__(self, dataset_dir, save_dir, random_seed=5252) -> None:
        random.seed(random_seed)
        
        self.dataset_dir = dataset_dir
        
        self.frame_save_path = os.path.join(save_dir, 'images')
        os.makedirs( self.frame_save_path, exist_ok=True)
        self.mask_save_path = os.path.join(save_dir, 'labels')
        os.makedirs(self.mask_save_path, exist_ok=True)

        img_dir = os.path.join(dataset_dir, 'images')
        mask_dir = os.path.join(dataset_dir, 'labels')
        img_names = [name for name in os.listdir(img_dir) if name.endswith('.jp2')]
        self.img_paths = [os.path.join(img_dir, name) for name in img_names]
        self.mask_paths = [os.path.join(mask_dir, name) for name in img_names]
        
        self.frame_size = int(os.getenv('FRAME_SIZE'))
        self.min_proportion = float(os.getenv('MIN_MOSAIC_PROPORTION'))
        self.max_proportion = float(os.getenv('MAX_MOSAIC_PROPORTION'))
        self.max_selection_count = int(os.getenv('MAX_SELECTION_COUNT'))
        self.pad_size = 50

        self.frame = None
        self.src = None

    def start_augmentation(self, start_idx: Union[None, int] = None, 
                           end_idx: Union[None, int] = None):
    
        for img_path, mask_path in zip(tqdm(self.img_paths[start_idx:end_idx]), self.mask_paths[start_idx:end_idx]):
            x_offset, y_offset, x_proportion, y_proportion = self._get_offset()
            
            window = ((y_offset - self.pad_size, self.frame_size + 2*self.pad_size),
                      (x_offset - self.pad_size, self.frame_size + 2*self.pad_size))
            
            self.frame, self.src = load_frame(img_path, mask_path, window=window)
            coords = generate_coords(self.src.height, self.src.width, x_offset, y_offset, x_proportion, y_proportion)
            
            for coord in coords:
                self._enter_other_frame(coord)
                
            self._save_augmented_frame()

    def _get_offset(self):
        x_proportion = random.uniform(self.min_proportion, self.max_proportion)
        y_proportion = random.uniform(self.min_proportion, self.max_proportion)
        x_offset = round(self.frame_size * x_proportion)
        y_offset = round(self.frame_size * y_proportion)
        x_offset = x_offset if random.uniform(0, 1) > 0.5 else -1 * x_offset
        y_offset = y_offset if random.uniform(0, 1) > 0.5 else -1 * y_offset
        return x_offset, y_offset, x_proportion, y_proportion
    
    def _enter_other_frame(self, coord):
        img_idx = random.randint(0, len(self.img_paths)-1)
        
        for _ in range(self.max_selection_count):
            y_offset = random.randint(0, self.frame_size-coord.y_len)
            x_offset = random.randint(0, self.frame_size-coord.x_len)
            
            other_frame, _ = load_frame(self.img_paths[img_idx], self.mask_paths[img_idx], 
                                    window=((y_offset, coord.y_len),(x_offset, coord.x_len)))
            
            if np.any(other_frame[-1:,...]==1):
                break
        _, y_len, x_len = other_frame.shape
        self.frame[:, 
                coord.y_start + self.pad_size : coord.y_start+y_len+self.pad_size,
                coord.x_start + self.pad_size: coord.x_start+x_len+self.pad_size] = other_frame         

    def _save_augmented_frame(self):
        self.frame = self.frame [..., self.pad_size:-self.pad_size, self.pad_size:-self.pad_size] 
        frame_name = f'augmented_{uuid.uuid4().hex}.jp2'
        frame = self.frame[:-1,...]
        mask = self.frame[-1:,...]
        
        profile = self.src.profile
        profile.update({
            'QUALITY': '100',  # Отвечает за сохранение без потери данных
            'REVERSIBLE': 'YES',  # Отвечает за сохранение без потери данных
            'YCBCR420': 'NO',  # Отвечает за сохранение без потери данных
            'count': frame.shape[0],
        })      
        
        with rasterio.open(os.path.join(self.frame_save_path, frame_name), 
                           mode='w', **profile) as dst:
            dst.write(frame)
            
        profile.update({
            'count': mask.shape[0]
        })      
        
        with rasterio.open(os.path.join(self.mask_save_path, frame_name), 
                           mode='w', **profile) as dst:
            dst.write(mask)
        
# x_start = x_start,
# y_start = y_start,
# x_len = x_len,
# y_len = y_len