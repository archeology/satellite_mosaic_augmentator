import os
import random
from typing import Iterator
from dataclasses import dataclass

import numpy as np


@dataclass
class AppendCoord:
    x_start: int
    y_start: int
    x_len: int
    y_len: int   


def generate_coords(height: int, width: int, x_img: int, y_img: int,
                    min_x_proportion: int, min_y_proportion: int) -> Iterator[AppendCoord]:

    max_proportion = float(os.getenv('MAX_MOSAIC_PROPORTION'))
    
    x_len = width * random.uniform(min_x_proportion, max_proportion)
    y_len = height * random.uniform(min_y_proportion, max_proportion)
    x_len = round(x_len)
    y_len = round(y_len)
    
    reverse_x = True if x_img < 0  else False
    reverse_y = True if y_img < 0  else False
    
    x_start = 0 if reverse_x else width - x_len
    y_start = 0 if reverse_y else height - y_len
    
    yield AppendCoord(
        x_start = x_start,
        y_start = y_start,
        x_len = x_len,
        y_len = y_len
    )
    
    x_processed_flag = True
    y_processed_flag = True
    base_x_start = x_start
    base_y_start = y_start

    while x_processed_flag:
        x_processed_flag, x_start, x_len = _get_coord(width, x_start, x_len, reverse_x, min_x_proportion)            
        yield AppendCoord(
            x_start = x_start,
            y_start = base_y_start,
            x_len = x_len,
            y_len = round(height * random.uniform(min_x_proportion, max_proportion))
        )
           
    while y_processed_flag:
        y_processed_flag, y_start, y_len = _get_coord(height, y_start, y_len, reverse_y, min_y_proportion)            
        yield AppendCoord(
            x_start = base_x_start,
            y_start = y_start,
            x_len = round(width * random.uniform(min_y_proportion, max_proportion)),
            y_len = y_len
        )

def _get_coord(max_len, start_coord, object_len, reverse_coord, min_proportion):
    max_proportion = float(os.getenv('MAX_MOSAIC_PROPORTION'))
    processed_flag = True

    if reverse_coord:
        start_coord = start_coord + object_len
        object_len = max_len * random.uniform(min_proportion, max_proportion)

        if start_coord + object_len == max_len:
            processed_flag = True
            
        if start_coord + object_len > max_len:
            object_len = object_len - (start_coord + object_len - max_len)
            processed_flag = False

    else:
        object_len = max_len * random.uniform(min_proportion, max_proportion)

        if start_coord - object_len == 0:
            processed_flag = True
        if start_coord - object_len < 0:
            object_len = object_len + (start_coord - object_len)
            processed_flag = False
        start_coord = start_coord - object_len
    
    return processed_flag, round(start_coord), round(object_len)