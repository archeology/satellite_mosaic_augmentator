from typing import Tuple, Union

import rasterio
from rasterio.windows import Window
import numpy as np

def load_frame(img_path: str, mask_path: str, 
               window: Union[None, Tuple[Tuple[int,int], Tuple[int,int]]] = None) -> np.ndarray:
    """ Загрузка кадра и маски рубок
    Args:
        img_path (str): путь к кадру
        mask_path (str):  путь к маске кадру
        window: Union[None, Tuple[Tuple[int,int], Tuple[int,int]]: 
        рамка в границах которой требуется загрущить кадр: ((y_min, y_len),(x_min, x_len)), если не указан - весь кадр
    Returns:
        Tuple(np.ndarray, rasterio.io.DatasetReader): (кадр в формате (C, H. W), 
        где [:-1, ...] - кадр
        где [-1, ...] - маска;
        метаданные кадра
    """
    
    img_src = rasterio.open(img_path)
    mask_src = rasterio.open(mask_path)
    
    if window is not None:
        window = Window(col_off=window[1][0], row_off=window[0][0],
                        width=window[1][1], height=window[0][1])
        img =  img_src.read(window = window, boundless=True)
        mask = mask_src.read(window = window, boundless=True)
    else:
        img =  img_src.read()
        mask = mask_src.read()
        
    frame = np.concatenate((img, mask), axis=0)
    return frame, img_src


